﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;

public class DataManager : MonoBehaviour {

    public InputField nameInput;
    public InputField emailInput;
    public InputField phoneInput;
    public ScriptableScore scoreFile;
    public Users usersCard;
    public SaveSystem ss;

    private const string databaseURL = "http://190905-my-honda.unicom-interactive-digital.com/submit-data.php";

    float timeS = 0f;
    bool OnClick = false;

    public Text sentText;
    private int totalSent;
    public GameObject emptyHandler;
    public GameObject ErrorHandler;
    public GameObject LoadingHandler;
    public GameObject SuccessSendDataHandler;
    public GameObject blockDataHandler;

    public void ClearTempUser()
    {
        usersCard.tempUser = new User();
    }

    public void SaveToLocal()
    {
        usersCard.tempUser.name = nameInput.text.ToString();
        usersCard.tempUser.email = emailInput.text.ToString();
        usersCard.tempUser.phone = phoneInput.text.ToString();
        usersCard.tempUser.register_datetime = System.DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");

        
    }

    public void SaveScoreToLocal()
    {
        usersCard.tempUser.score = scoreFile.score.ToString();

        string userType;

        /*
        if (CheckSameUser(usersCard.users, usersCard.tempUser) != "not")
        {
            userType = CheckSameUser(usersCard.users, usersCard.tempUser);
            // user already exists
            User sameUser = GetSameUser(usersCard.users, usersCard.tempUser);
            UpdateSameUserScore(sameUser, usersCard.tempUser);
        }
        else if (CheckSameUser(usersCard.syncUser, usersCard.tempUser) != "not" )
        {
            // user already in remote database exists
            User sameUser = GetSameUser(usersCard.syncUser, usersCard.tempUser);

            if (UpdateSameUserScore(sameUser, usersCard.tempUser))
            {
                // add user to update list
                usersCard.AddUserToUpdateList(sameUser);
            }
        }
        else
        {     
            usersCard.SaveData(usersCard.tempUser);
        }

    */

        ss.SavePlayer();

        ClearTempUser();
    }

    private string CheckSameUser(List<User> usersList, User tempUser)
    {
        string isSame = "not";

        for (int i = 0; i < usersList.Count; i++)
        {
            // if the email & phone is same
            if (usersList[i].email == tempUser.email) {
                isSame = "email";
            } 
            if(usersList[i].phone == tempUser.phone)
            {
                isSame = "phone";
            }
        }

        return isSame;      
    }

    public string CheckSameUserInput()
    {
        string type = "";

        for (int i = 0; i < usersCard.users.Count; i++)
        {
            // if the email & phone is same
            if (emailInput.text == usersCard.users[i].email)
            {
                type = "email";
            }
            if (phoneInput.text == usersCard.users[i].phone)
            {
                type = "phone";
            }
        }

        return type;
    }

    public bool CheckSameUserEmail()
    {
        bool emailIsSame = false;

        for (int i = 0; i < usersCard.users.Count; i++)
        {
            // if the email & phone is same
            if (emailInput.text == usersCard.users[i].email)
            {
                emailIsSame = true;
            }
        }

            return emailIsSame;
    }

    public bool CheckSameUserPhone()
    {
        bool phoneIsSame = false;

        for (int i = 0; i < usersCard.users.Count; i++)
        {
            // if the email & phone is same
            if (phoneInput.text == usersCard.users[i].phone)
            {
                phoneIsSame = true;
            }
        }

        return phoneIsSame;
    }

    private User GetSameUser(List<User> usersList, User tempUser)
    {
        for (int i = 0; i < usersList.Count; i++)
        {
            // if the email & phone is same
            if (usersList[i].email == tempUser.email &&
            usersList[i].phone == tempUser.phone)
            {
                return usersList[i];
            }
        }
        return null;
    }

    private bool UpdateSameUserScore(User oldData, User newData)
    {
        bool isUpdated = false;

        if (int.Parse(oldData.score) < int.Parse(newData.score))
        {
            oldData.score = newData.score;
            isUpdated = true;
        }

        return isUpdated;
    }

    public void SendDataToDatabase()
    {
        StartCoroutine(DataToSend());
    }

    public void Show()
    {

    }

    IEnumerator DataToSend()
    {
        Debug.Log("File Path: " + Application.persistentDataPath + "/LocalData.txt");
        ///////////////////////
        List<User> unSyncUsers = new List<User>();
        unSyncUsers = usersCard.GetUnSync(ss.LoadPlayer(), ss.LoadSyncedPlayer());
        totalSent = 0;

        

        if(unSyncUsers.Count < 1)
        {
            emptyHandler.SetActive(true);
            yield break;
        }
        else
        {
            emptyHandler.SetActive(false);
            blockDataHandler.SetActive(true);
        }
        ////////////////////////
        Debug.Log(unSyncUsers.Count +" unsync");
        for (int i = 0; i < unSyncUsers.Count; i++)
        {
            WWWForm form = new WWWForm();
            form.AddField("name", unSyncUsers[i].name);
            form.AddField("email", unSyncUsers[i].email);
            form.AddField("phone", unSyncUsers[i].phone);
            form.AddField("score", unSyncUsers[i].score);
            form.AddField("register_datetime", unSyncUsers[i].register_datetime);
            

            using (UnityWebRequest www = UnityWebRequest.Post(databaseURL, form))
            {
                
                LoadingHandler.SetActive(true);
                yield return www.SendWebRequest();

                if (www.isNetworkError || www.isHttpError)
                {
                    Debug.Log(www.error);
                    ErrorHandler.SetActive(true);
                }
                else
                {                  
                    var jsonData = JsonUtility.FromJson<JSONResponse>(www.downloadHandler.text);
                    Debug.Log(jsonData.result);
                    totalSent++;
                    sentText.text = totalSent.ToString();
                    usersCard.SyncUser(unSyncUsers[i]);
                    usersCard.userToSync.Remove(unSyncUsers[i]);

                    ss.SaveSyncPlayer(unSyncUsers[i]);

                    SuccessSendDataHandler.SetActive(true);
                }
            }
            LoadingHandler.SetActive(false);

            yield return new WaitForSeconds(0.2f);
        }

        string TextPath = Application.persistentDataPath + "/LocalData.txt";
        // reset users
        File.WriteAllText(TextPath, "");

        blockDataHandler.SetActive(false);
    }

}

public class JSONResponse
{
    public string result;
}
