﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "users", menuName ="users")]
public class Users : ScriptableObject
{
    public List<User> users;
    public List<User> syncUser;
    public List<User> userToSync;
    public List<User> userToUpdate;

    public User tempUser;

    [SerializeField]
    private int playAmount;

    public void SaveData(User user)
    {
        playAmount++;
        user.ID = playAmount;

        users.Add(user);
        
    }

    public List<User> GetUnSync(List<User> users_, List<User> syncUser_)
    {
        ClearUserToSync();

       // List<User> unSyncUser = new List<User>();
        List<User> unSyncUser = users_;

        for (int i = 0; i < users.Count; i++)
        {
            if (users[i].is_sync == "false") {
                userToSync.Add(users[i]);               
            }
        } 

        foreach(User u in userToSync)
        {
            unSyncUser.Add(u);
        }

        syncUser = syncUser_;
        

        for (int s = 0; s < syncUser.Count; s++)
        {
            foreach (User toSync in userToSync)
            {
                if(toSync.email == syncUser[s].email && toSync.phone == syncUser[s].phone)
                {
                    unSyncUser.Remove(toSync);
                }
            }
        }

        return unSyncUser;
    }

    public void ClearUserToSync()
    {
        userToSync.Clear();
    }

    public void SyncUser(User user)
    {
        user.Sync();
        syncUser.Add(user);
    }

    public void UpdateUserScore(User user, int score)
    {
        user.score = score.ToString();
    }

    public void AddUserToUpdateList(User user_)
    {
        userToUpdate.Add(user_);
    }
}
